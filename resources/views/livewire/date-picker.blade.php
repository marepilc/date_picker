<div class="flex justify-center select-none">
    <div
        x-data="{
            yearIsOpen: false,
            monthIsOpen: false,
            dayIsOpen: false,
            yearToggle() {
                this.yearIsOpen = !this.yearIsOpen;
            },
            yearClose($nextPanel = '') {
                if (!this.yearIsOpen) return;
                this.yearIsOpen = false;
                if ($nextPanel === 'month') this.monthIsOpen = true;
            },
            monthToggle() {
                this.monthIsOpen = !this.monthIsOpen;
            },
            monthClose($nextPanel = '') {
                if (!this.monthIsOpen) return;
                this.monthIsOpen = false;
                if ($nextPanel === 'day') this.dayIsOpen = true;
            },
            dayToggle() {
                this.dayIsOpen = !this.dayIsOpen;
            },
            dayClose() {
                if (!this.dayIsOpen) return;
                this.dayIsOpen = false;
            },
            closeAll() {
                this.yearIsOpen = false;
                this.monthIsOpen = false;
            }
        }"
        x-id="['year-dropdown', 'month-dropdown', 'day-dropdown']"
        @keydown.esc.prevent.stop="closeAll()"
        class="relative"
    >
        {{-- Data Picker Frame --}}
        <div
            class="border border-solid border-stone-400 bg-white px-4 py-1 rounded-md cursor-default"
        >
            <button
                :aria-controls="$id('year-dropdown')"
                @click="yearToggle()"
                type="button"
                class="inline-block hover:font-semibold px-1"
            >
                {{ $year }}
            </button>
            <div class="inline-block">-</div>
            <button
                :aria-controls="$id('monh-dropdown')"
                @click="monthToggle()"
                type="button"
                class="inline-block hover:font-semibold px-1"
            >
                {{ $this->twoDigit($month) }}
            </button>
            <div class="inline-block">-</div>
            <button
                :aria-controls="$id('month-dropdown')"
                @click="dayToggle()"
                type="button"
                class="inline-block hover:font-semibold px-1"
            >
                {{ $this->twoDigit($day) }}
            </button>
        </div>

        {{-- Year Panel --}}
        <div
            :id="$id('year-dropdown')"
            style="display: none;"
            x-show="yearIsOpen"
            @click.outside="yearClose()"
            class="absolute left-1/2 -translate-x-1/2 w-60 border border-solid border-stone-400
                   rounded-md bg-white mt-1 shadow"
        >
            <ul class="grid grid-cols-3 text-center">
                @foreach($years as $y)
                    <li
                        @click="yearClose('month')"
                        wire:click="$set('year', {{ $years[$loop->index] }})"
                        class="inline-block py-1 hover:bg-stone-200 rounded-md cursor-pointer"
                    >
                        {{ $y }}
                    </li>
                @endforeach
            </ul>
        </div>
        {{-- Month Panel --}}
        <div
            :id="$id('month-dropdown')"
            style="display: none;"
            x-show="monthIsOpen"
            @click.outside="monthClose()"
            class="absolute left-1/2 -translate-x-1/2 w-80 border border-solid border-stone-400
                   rounded-md bg-white mt-1 shadow"

        >
            <ul class="grid grid-cols-3 text-center">
                @foreach($months as $m)
                    <li
                        @click="monthClose('day')"
                        wire:click="$set('month', {{ $loop->iteration }})"
                        class="inline-block py-1 hover:bg-stone-200 rounded-md cursor-pointer"
                    >
                        {{ $m }}
                    </li>
                @endforeach
            </ul>
        </div>
        {{-- Day Panel --}}
        <div
            :id="$id('day-dropdown')"
            style="display: none;"
            x-show="dayIsOpen"
            @click.outside="dayClose()"
            class="absolute flex justify-center left-1/2 -translate-x-1/2 w-72 border border-solid border-stone-400
                   rounded-md bg-white mt-1 py-2 shadow cursor-default"
        >
            {{-- Week No. Column --}}
            <div class="grid grid-cols-1 px-1 mr-2">
                <div>&nbsp;</div>
                @foreach($this->generateWeekNoList($year, $month) as $wno)
                    <div class="text-stone-300 text-right">{{ $wno }}</div>
                @endforeach
            </div>
            {{-- Calendar Column --}}
            <div>
                <div class="flex items-center">
                    @foreach($weekDayHeaders as $wdh)
                        <div class="px-2 w-8 font-bold text-center
                                @if($loop->iteration == 7)
                                    text-red-600
                                @endif "
                        >{{ $wdh }}</div>
                    @endforeach
                </div>
                <div class="grid grid-cols-7">
                    @foreach($this->generateDayList($year, $month) as $d)
                        @if($d != 0)
                            <div
                                @click="dayClose()"
                                wire:click="$set('day', {{ $d }})"
                                class="text-center hover:bg-stone-200 cursor-pointer
                                @if($loop->iteration % 7 == 0)
                                    text-red-600
                                @endif "
                            >{{ $d }}</div>
                        @else
                            <div class="text-center">&nbsp;</div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
