<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style type="text/css">
            #code1 .s0 { color: #cc7832;}
            #code1 .s1 { color: #a9b7c6;}
            #code1 .s2 { color: #a9b7c6;}
            #code1 .s3 { color: #9876aa;}
            #code1 .s4 { color: #6a8759;}
            #code1 .s5 { color: #6897bb;}
            #code1 .ls0 { height: 1px; border-width: 0; color: #4d4d4d; background-color:#4d4d4d}
            #code1 .ln { color: #606366; font-weight: normal; font-style: normal; }
            #code2 .s0 { color: #e8bf6a;}
            #code2 .s1 { color: #bababa;}
            #code2 .s2 { color: #a5c261;}
            #code2 .s3 { color: #a9b7c6;}
            #code2 .s4 { color: #808080; font-style: italic;}
            #code2 .s5 { color: #a9b7c6;}
            #code2 .s6 { color: #9876aa;}
            #code2 .s7 { color: #a9b7c6;}
            #code2 .s8 { color: #cc7832;}
            #code2 .s9 { color: #cc7832;}
            #code2 .s10 { color: #6d9cbe;}
            #code2 .s11 { color: #6897bb;}
            #code2 .ln { color: #606366; font-weight: normal; font-style: normal; }
        </style>
        <title>Datepicker</title>
        <link href="/css/app.css" rel="stylesheet">
    @livewireStyles
    </head>
    <body class="text-stone-700 bg-stone-50">
        <section class="container w-full justify-center mx-auto">
            <h1 class="w-full justify-center text-center text-4xl font-semibold mt-4 mb-10">The Datepicker Component</h1>
            <div class="w-9/12 justify-center mb-10 mx-auto">
                <img class="object-center" src="/img/tallstack.png" alt="tall stack">
            </div>

            @livewire('date-picker', [
                'yearRange' => [2022, 2014]
            ])

        </section>
        <section class="container w-full justify-center pt-20 mx-auto mb-20">
            <!-- Tabs -->
            <div
                x-data="{
                    selectedId: null,
                    init() {
                        // Set the first available tab on the page on page load.
                        this.$nextTick(() => this.select(this.$id('tab', 1)))
                    },
                    select(id) {
                        this.selectedId = id
                    },
                    isSelected(id) {
                        return this.selectedId === id
                    },
                    whichChild(el, parent) {
                        return Array.from(parent.children).indexOf(el) + 1
                    }
                }"
                x-id="['tab']"
            >
                <!-- Tab List -->
                <ul
                    x-ref="tablist"
                    @keydown.right.prevent.stop="$focus.wrap().next()"
                    @keydown.home.prevent.stop="$focus.first()"
                    @keydown.page-up.prevent.stop="$focus.first()"
                    @keydown.left.prevent.stop="$focus.wrap().prev()"
                    @keydown.end.prevent.stop="$focus.last()"
                    @keydown.page-down.prevent.stop="$focus.last()"
                    role="tablist"
                    class="-mb-px flex items-stretch"
                >
                    <!-- Tab -->
                    <li>
                        <button
                            :id="$id('tab', whichChild($el.parentElement, $refs.tablist))"
                            @click="select($el.id)"
                            @focus="select($el.id)"
                            type="button"
                            :tabindex="isSelected($el.id) ? 0 : -1"
                            :aria-selected="isSelected($el.id)"
                            :class="isSelected($el.id) ? 'border-stone-400 bg-stone-50' : 'border-transparent'"
                            class="inline-flex px-4 py-2 border-t border-l border-r rounded-t-md"
                            role="tab"
                        >DatePicker.php</button>
                    </li>

                    <li>
                        <button
                            :id="$id('tab', whichChild($el.parentElement, $refs.tablist))"
                            @click="select($el.id)"
                            @focus="select($el.id)"
                            type="button"
                            :tabindex="isSelected($el.id) ? 0 : -1"
                            :aria-selected="isSelected($el.id)"
                            :class="isSelected($el.id) ? 'border-stone-400 bg-stone-50' : 'border-transparent'"
                            class="inline-flex px-4 py-2 border-t border-l border-r rounded-t-md"
                            role="tab"
                        >date-picker.blade.php</button>
                    </li>
                </ul>

                <!-- Panels -->
                <div role="tabpanels" class="border border-stone-400 rounded-b-md">
                    <!-- Panel -->
                    <section
                        x-show="isSelected($id('tab', whichChild($el, $el.parentElement)))"
                        :aria-labelledby="$id('tab', whichChild($el, $el.parentElement))"
                        role="tabpanel"
                        class="p-8"
                    >
                        <pre class="bg-stone-800 px-4 py-2" id="code1"><a name="l1"><span class="ln">1    </span></a><span class="s0">&lt;?php</span>
<a name="l2"><span class="ln">2    </span></a><hr class="ls0"><a name="l3"><span class="ln">3    </span></a><span class="s0">namespace </span><span class="s2">App</span><span class="s1">\</span><span class="s2">Http</span><span class="s1">\</span><span class="s2">Livewire</span><span class="s0">;</span>
<a name="l4"><span class="ln">4    </span></a>
<a name="l5"><span class="ln">5    </span></a><span class="s0">use </span><span class="s2">Livewire</span><span class="s1">\</span><span class="s2">Component</span><span class="s0">;</span>
<a name="l6"><span class="ln">6    </span></a><hr class="ls0"><a name="l7"><span class="ln">7    </span></a><span class="s0">class </span><span class="s2">DatePicker </span><span class="s0">extends </span><span class="s2">Component</span>
<a name="l8"><span class="ln">8    </span></a><span class="s1">{</span>
<a name="l9"><span class="ln">9    </span></a>
<a name="l10"><span class="ln">10   </span></a>    <span class="s0">public array </span><span class="s3">$yearRange</span><span class="s0">;</span>
<a name="l11"><span class="ln">11   </span></a>    <span class="s0">public </span><span class="s2">int </span><span class="s3">$year</span><span class="s0">;</span>
<a name="l12"><span class="ln">12   </span></a>    <span class="s0">public </span><span class="s2">int </span><span class="s3">$month</span><span class="s0">;</span>
<a name="l13"><span class="ln">13   </span></a>    <span class="s0">public </span><span class="s2">int </span><span class="s3">$day</span><span class="s0">;</span>
<a name="l14"><span class="ln">14   </span></a>    <span class="s0">public array </span><span class="s3">$weekNumbers</span><span class="s0">;</span>
<a name="l15"><span class="ln">15   </span></a>    <span class="s0">public array </span><span class="s3">$selectedDays</span><span class="s0">;</span>
<a name="l16"><span class="ln">16   </span></a>
<a name="l17"><span class="ln">17   </span></a>
<a name="l18"><span class="ln">18   </span></a>    <span class="s0">public function </span><span class="s2">mount</span><span class="s1">()</span>
<a name="l19"><span class="ln">19   </span></a>    <span class="s1">{</span>
<a name="l20"><span class="ln">20   </span></a>        <span class="s3">$now </span><span class="s1">= </span><span class="s2">getdate</span><span class="s1">()</span><span class="s0">;</span>
<a name="l21"><span class="ln">21   </span></a>        <span class="s3">$this</span><span class="s1">-&gt;</span><span class="s2">year </span><span class="s1">= </span><span class="s3">$now</span><span class="s1">[</span><span class="s4">'year'</span><span class="s1">]</span><span class="s0">;</span>
<a name="l22"><span class="ln">22   </span></a>        <span class="s3">$this</span><span class="s1">-&gt;</span><span class="s2">month </span><span class="s1">= </span><span class="s3">$now</span><span class="s1">[</span><span class="s4">'mon'</span><span class="s1">]</span><span class="s0">;</span>
<a name="l23"><span class="ln">23   </span></a>        <span class="s3">$this</span><span class="s1">-&gt;</span><span class="s2">day </span><span class="s1">= </span><span class="s3">$now</span><span class="s1">[</span><span class="s4">'mday'</span><span class="s1">]</span><span class="s0">;</span>
<a name="l24"><span class="ln">24   </span></a>    <span class="s1">}</span>
<a name="l25"><span class="ln">25   </span></a>
<a name="l26"><span class="ln">26   </span></a>    <span class="s0">public function </span><span class="s2">updated</span><span class="s1">(</span><span class="s3">$name</span><span class="s0">, </span><span class="s3">$value</span><span class="s1">)</span>
<a name="l27"><span class="ln">27   </span></a>    <span class="s1">{</span>
<a name="l28"><span class="ln">28   </span></a>        <span class="s3">$this</span><span class="s1">-&gt;</span><span class="s2">selectedDays </span><span class="s1">= </span><span class="s3">$this</span><span class="s1">-&gt;</span><span class="s2">generateDayList</span><span class="s1">(</span><span class="s3">$this</span><span class="s1">-&gt;</span><span class="s2">year</span><span class="s0">, </span><span class="s3">$this</span><span class="s1">-&gt;</span><span class="s2">month</span><span class="s1">)</span><span class="s0">;</span>
<a name="l29"><span class="ln">29   </span></a>    <span class="s1">}</span>
<a name="l30"><span class="ln">30   </span></a>
<a name="l31"><span class="ln">31   </span></a>    <span class="s0">public function </span><span class="s2">render</span><span class="s1">()</span>
<a name="l32"><span class="ln">32   </span></a>    <span class="s1">{</span>
<a name="l33"><span class="ln">33   </span></a>
<a name="l34"><span class="ln">34   </span></a>        <span class="s0">return </span><span class="s2">view</span><span class="s1">(</span><span class="s4">'livewire.date-picker'</span><span class="s0">, </span><span class="s1">[</span>
<a name="l35"><span class="ln">35   </span></a>            <span class="s4">'years' </span><span class="s1">=&gt; </span><span class="s3">$this</span><span class="s1">-&gt;</span><span class="s2">generateYearList</span><span class="s1">(</span><span class="s3">$this</span><span class="s1">-&gt;</span><span class="s2">yearRange</span><span class="s1">)</span><span class="s0">,</span>
<a name="l36"><span class="ln">36   </span></a>            <span class="s4">'months' </span><span class="s1">=&gt; [</span>
<a name="l37"><span class="ln">37   </span></a>                <span class="s4">'January'</span><span class="s0">, </span><span class="s4">'February'</span><span class="s0">, </span><span class="s4">'March'</span><span class="s0">,</span>
<a name="l38"><span class="ln">38   </span></a>                <span class="s4">'April'  </span><span class="s0">, </span><span class="s4">'May'     </span><span class="s0">, </span><span class="s4">'June'</span><span class="s0">,</span>
<a name="l39"><span class="ln">39   </span></a>                <span class="s4">'July'   </span><span class="s0">, </span><span class="s4">'August'  </span><span class="s0">, </span><span class="s4">'September'</span><span class="s0">,</span>
<a name="l40"><span class="ln">40   </span></a>                <span class="s4">'October'</span><span class="s0">, </span><span class="s4">'November'</span><span class="s0">, </span><span class="s4">'December'</span>
<a name="l41"><span class="ln">41   </span></a>            <span class="s1">]</span><span class="s0">,</span>
<a name="l42"><span class="ln">42   </span></a>            <span class="s4">'weekDayHeaders' </span><span class="s1">=&gt; [</span><span class="s4">'M'</span><span class="s0">, </span><span class="s4">'T'</span><span class="s0">, </span><span class="s4">'W'</span><span class="s0">, </span><span class="s4">'T'</span><span class="s0">, </span><span class="s4">'F'</span><span class="s0">, </span><span class="s4">'S'</span><span class="s0">, </span><span class="s4">'S'</span><span class="s1">]</span><span class="s0">,</span>
<a name="l43"><span class="ln">43   </span></a>        <span class="s1">])</span><span class="s0">;</span>
<a name="l44"><span class="ln">44   </span></a>    <span class="s1">}</span>
<a name="l45"><span class="ln">45   </span></a>
<a name="l46"><span class="ln">46   </span></a>    <span class="s0">private function </span><span class="s2">generateYearList</span><span class="s1">(</span><span class="s3">$r</span><span class="s1">): </span><span class="s0">array </span><span class="s1">{</span>
<a name="l47"><span class="ln">47   </span></a>        <span class="s0">return </span><span class="s2">range</span><span class="s1">(</span><span class="s3">$r</span><span class="s1">[</span><span class="s5">0</span><span class="s1">]</span><span class="s0">, </span><span class="s3">$r</span><span class="s1">[</span><span class="s5">1</span><span class="s1">])</span><span class="s0">;</span>
<a name="l48"><span class="ln">48   </span></a>    <span class="s1">}</span>
<a name="l49"><span class="ln">49   </span></a>
<a name="l50"><span class="ln">50   </span></a>    <span class="s0">private function </span><span class="s2">generateDayList</span><span class="s1">(</span><span class="s3">$y</span><span class="s0">, </span><span class="s3">$m</span><span class="s1">): </span><span class="s0">array </span><span class="s1">{</span>
<a name="l51"><span class="ln">51   </span></a>        <span class="s3">$daysCount </span><span class="s1">= </span><span class="s2">cal_days_in_month</span><span class="s1">(</span><span class="s2">CAL_GREGORIAN</span><span class="s0">, </span><span class="s3">$m</span><span class="s0">, </span><span class="s3">$y</span><span class="s1">)</span><span class="s0">;</span>
<a name="l52"><span class="ln">52   </span></a>        <span class="s3">$firstDay </span><span class="s1">= </span><span class="s2">mktime</span><span class="s1">(</span><span class="s5">0</span><span class="s0">, </span><span class="s5">0</span><span class="s0">, </span><span class="s5">0</span><span class="s0">, </span><span class="s3">$m</span><span class="s0">, </span><span class="s5">1</span><span class="s0">, </span><span class="s3">$y</span><span class="s1">)</span><span class="s0">;</span>
<a name="l53"><span class="ln">53   </span></a>        <span class="s3">$daysToAdd </span><span class="s1">= [</span><span class="s5">6</span><span class="s0">, </span><span class="s5">0</span><span class="s0">, </span><span class="s5">1</span><span class="s0">, </span><span class="s5">2</span><span class="s0">, </span><span class="s5">3</span><span class="s0">, </span><span class="s5">4</span><span class="s0">, </span><span class="s5">5</span><span class="s1">][</span><span class="s2">getdate</span><span class="s1">(</span><span class="s3">$firstDay</span><span class="s1">)[</span><span class="s4">'wday'</span><span class="s1">]]</span><span class="s0">;</span>
<a name="l54"><span class="ln">54   </span></a>        <span class="s3">$result </span><span class="s1">= </span><span class="s2">range</span><span class="s1">(</span><span class="s5">1</span><span class="s0">, </span><span class="s3">$daysCount</span><span class="s1">)</span><span class="s0">;</span>
<a name="l55"><span class="ln">55   </span></a>        <span class="s0">for </span><span class="s1">(</span><span class="s3">$i </span><span class="s1">= </span><span class="s5">0</span><span class="s0">; </span><span class="s3">$i </span><span class="s1">&lt; </span><span class="s3">$daysToAdd</span><span class="s0">; </span><span class="s3">$i</span><span class="s1">++) {</span>
<a name="l56"><span class="ln">56   </span></a>            <span class="s2">array_unshift</span><span class="s1">(</span><span class="s3">$result</span><span class="s0">, </span><span class="s5">0</span><span class="s1">)</span><span class="s0">;</span>
<a name="l57"><span class="ln">57   </span></a>        <span class="s1">}</span>
<a name="l58"><span class="ln">58   </span></a>        <span class="s0">return </span><span class="s3">$result </span><span class="s0">;</span>
<a name="l59"><span class="ln">59   </span></a>    <span class="s1">}</span>
<a name="l60"><span class="ln">60   </span></a>
<a name="l61"><span class="ln">61   </span></a>    <span class="s0">private function </span><span class="s2">generateWeekNoList</span><span class="s1">(</span><span class="s3">$y</span><span class="s0">, </span><span class="s3">$m</span><span class="s1">): </span><span class="s0">array </span><span class="s1">{</span>
<a name="l62"><span class="ln">62   </span></a>        <span class="s3">$firstWeek </span><span class="s1">= (int)</span><span class="s2">idate</span><span class="s1">(</span><span class="s4">'W'</span><span class="s0">, </span><span class="s2">mktime</span><span class="s1">(</span><span class="s5">0</span><span class="s0">, </span><span class="s5">0</span><span class="s0">, </span><span class="s5">0</span><span class="s0">, </span><span class="s3">$m</span><span class="s0">, </span><span class="s5">1</span><span class="s0">, </span><span class="s3">$y</span><span class="s1">))</span><span class="s0">;</span>
<a name="l63"><span class="ln">63   </span></a>        <span class="s3">$fieldsCount </span><span class="s1">= </span><span class="s2">count</span><span class="s1">(</span><span class="s3">$this</span><span class="s1">-&gt;</span><span class="s2">generateDayList</span><span class="s1">(</span><span class="s3">$y</span><span class="s0">, </span><span class="s3">$m</span><span class="s1">))</span><span class="s0">;</span>
<a name="l64"><span class="ln">64   </span></a>        <span class="s0">return </span><span class="s2">range</span><span class="s1">(</span><span class="s3">$firstWeek</span><span class="s0">, </span><span class="s3">$firstWeek </span><span class="s1">+ </span><span class="s2">intdiv</span><span class="s1">(</span><span class="s3">$fieldsCount</span><span class="s0">, </span><span class="s5">7</span><span class="s1">))</span><span class="s0">;</span>
<a name="l65"><span class="ln">65   </span></a>    <span class="s1">}</span>
<a name="l66"><span class="ln">66   </span></a>
<a name="l67"><span class="ln">67   </span></a>    <span class="s0">private function </span><span class="s2">twoDigit</span><span class="s1">(</span><span class="s2">int </span><span class="s3">$v</span><span class="s1">): </span><span class="s2">string </span><span class="s1">{</span>
<a name="l68"><span class="ln">68   </span></a>        <span class="s0">return </span><span class="s1">(</span><span class="s3">$v </span><span class="s1">&lt; </span><span class="s5">10</span><span class="s1">) ? </span><span class="s4">'0'</span><span class="s1">.</span><span class="s3">$v </span><span class="s1">: (string)</span><span class="s3">$v</span><span class="s0">;</span>
<a name="l69"><span class="ln">69   </span></a>    <span class="s1">}</span>
<a name="l70"><span class="ln">70   </span></a><span class="s1">}</span></pre>
{{--                        <h2 class="font-bold text-xl">Tab 1 Content</h2>--}}
{{--                        <p class="mt-2 text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio, quo sequi error quibusdam quas temporibus animi sapiente eligendi! Deleniti minima velit recusandae iure.</p>--}}
                    </section>

                    <section
                        x-show="isSelected($id('tab', whichChild($el, $el.parentElement)))"
                        :aria-labelledby="$id('tab', whichChild($el, $el.parentElement))"
                        role="tabpanel"
                        class="p-8"
                    >
                        <pre class="bg-stone-800 px-4 py-2" id="code2"><a name="l1"><span class="ln">1    </span></a><span class="s0">&lt;div </span><span class="s1">class</span><span class="s2">=&quot;flex justify-center select-none&quot;</span><span class="s0">&gt;</span>
<a name="l2"><span class="ln">2    </span></a>    <span class="s0">&lt;div</span>
<a name="l3"><span class="ln">3    </span></a>        <span class="s1">x-data</span><span class="s2">=&quot;{
<a name="l4"><span class="ln">4    </span></a>            yearIsOpen: false,
<a name="l5"><span class="ln">5    </span></a>            monthIsOpen: false,
<a name="l6"><span class="ln">6    </span></a>            dayIsOpen: false,
<a name="l7"><span class="ln">7    </span></a>            yearToggle() {
<a name="l8"><span class="ln">8    </span></a>                this.yearIsOpen = !this.yearIsOpen;
<a name="l9"><span class="ln">9    </span></a>            },
<a name="l10"><span class="ln">10   </span></a>            yearClose($nextPanel = '') {
<a name="l11"><span class="ln">11   </span></a>                if (!this.yearIsOpen) return;
<a name="l12"><span class="ln">12   </span></a>                this.yearIsOpen = false;
<a name="l13"><span class="ln">13   </span></a>                if ($nextPanel === 'month') this.monthIsOpen = true;
<a name="l14"><span class="ln">14   </span></a>            },
<a name="l15"><span class="ln">15   </span></a>            monthToggle() {
<a name="l16"><span class="ln">16   </span></a>                this.monthIsOpen = !this.monthIsOpen;
<a name="l17"><span class="ln">17   </span></a>            },
<a name="l18"><span class="ln">18   </span></a>            monthClose($nextPanel = '') {
<a name="l19"><span class="ln">19   </span></a>                if (!this.monthIsOpen) return;
<a name="l20"><span class="ln">20   </span></a>                this.monthIsOpen = false;
<a name="l21"><span class="ln">21   </span></a>                if ($nextPanel === 'day') this.dayIsOpen = true;
<a name="l22"><span class="ln">22   </span></a>            },
<a name="l23"><span class="ln">23   </span></a>            dayToggle() {
<a name="l24"><span class="ln">24   </span></a>                this.dayIsOpen = !this.dayIsOpen;
<a name="l25"><span class="ln">25   </span></a>            },
<a name="l26"><span class="ln">26   </span></a>            dayClose() {
<a name="l27"><span class="ln">27   </span></a>                if (!this.dayIsOpen) return;
<a name="l28"><span class="ln">28   </span></a>                this.dayIsOpen = false;
<a name="l29"><span class="ln">29   </span></a>            },
<a name="l30"><span class="ln">30   </span></a>            closeAll() {
<a name="l31"><span class="ln">31   </span></a>                this.yearIsOpen = false;
<a name="l32"><span class="ln">32   </span></a>                this.monthIsOpen = false;
<a name="l33"><span class="ln">33   </span></a>            }
<a name="l34"><span class="ln">34   </span></a>        }&quot;</span>
<a name="l35"><span class="ln">35   </span></a>        <span class="s1">x-id</span><span class="s2">=&quot;['year-dropdown', 'month-dropdown', 'day-dropdown']&quot;</span>
<a name="l36"><span class="ln">36   </span></a>        <span class="s1">&#64;keydown.esc.prevent.stop</span><span class="s2">=&quot;closeAll()&quot;</span>
<a name="l37"><span class="ln">37   </span></a>        <span class="s1">class</span><span class="s2">=&quot;relative&quot;</span>
<a name="l38"><span class="ln">38   </span></a>    <span class="s0">&gt;</span>
<a name="l39"><span class="ln">39   </span></a>        <span class="s4">&#123;&#123;-- Data Picker Frame --&#125;&#125;</span>
<a name="l40"><span class="ln">40   </span></a>        <span class="s0">&lt;div</span>
<a name="l41"><span class="ln">41   </span></a>            <span class="s1">class</span><span class="s2">=&quot;border border-solid border-stone-400 bg-white px-4 py-1 rounded-md cursor-default&quot;</span>
<a name="l42"><span class="ln">42   </span></a>        <span class="s0">&gt;</span>
<a name="l43"><span class="ln">43   </span></a>            <span class="s0">&lt;button</span>
<a name="l44"><span class="ln">44   </span></a>                <span class="s1">:aria-controls</span><span class="s2">=&quot;$id('year-dropdown')&quot;</span>
<a name="l45"><span class="ln">45   </span></a>                <span class="s1">&#64;click</span><span class="s2">=&quot;yearToggle()&quot;</span>
<a name="l46"><span class="ln">46   </span></a>                <span class="s1">type</span><span class="s2">=&quot;button&quot;</span>
<a name="l47"><span class="ln">47   </span></a>                <span class="s1">class</span><span class="s2">=&quot;inline-block hover:font-semibold px-1&quot;</span>
<a name="l48"><span class="ln">48   </span></a>            <span class="s0">&gt;</span>
<a name="l49"><span class="ln">49   </span></a>                <span class="s5">&#123;&#123; </span><span class="s6">$year </span><span class="s5">&#125;&#125;</span>
<a name="l50"><span class="ln">50   </span></a>            <span class="s0">&lt;/button&gt;</span>
<a name="l51"><span class="ln">51   </span></a>            <span class="s0">&lt;div </span><span class="s1">class</span><span class="s2">=&quot;inline-block&quot;</span><span class="s0">&gt;</span><span class="s3">-</span><span class="s0">&lt;/div&gt;</span>
<a name="l52"><span class="ln">52   </span></a>            <span class="s0">&lt;button</span>
<a name="l53"><span class="ln">53   </span></a>                <span class="s1">:aria-controls</span><span class="s2">=&quot;$id('monh-dropdown')&quot;</span>
<a name="l54"><span class="ln">54   </span></a>                <span class="s1">&#64;click</span><span class="s2">=&quot;monthToggle()&quot;</span>
<a name="l55"><span class="ln">55   </span></a>                <span class="s1">type</span><span class="s2">=&quot;button&quot;</span>
<a name="l56"><span class="ln">56   </span></a>                <span class="s1">class</span><span class="s2">=&quot;inline-block hover:font-semibold px-1&quot;</span>
<a name="l57"><span class="ln">57   </span></a>            <span class="s0">&gt;</span>
<a name="l58"><span class="ln">58   </span></a>                <span class="s5">&#123;&#123; </span><span class="s6">$this</span><span class="s5">-&gt;</span><span class="s7">twoDigit</span><span class="s5">(</span><span class="s6">$month</span><span class="s5">) &#125;&#125;</span>
<a name="l59"><span class="ln">59   </span></a>            <span class="s0">&lt;/button&gt;</span>
<a name="l60"><span class="ln">60   </span></a>            <span class="s0">&lt;div </span><span class="s1">class</span><span class="s2">=&quot;inline-block&quot;</span><span class="s0">&gt;</span><span class="s3">-</span><span class="s0">&lt;/div&gt;</span>
<a name="l61"><span class="ln">61   </span></a>            <span class="s0">&lt;button</span>
<a name="l62"><span class="ln">62   </span></a>                <span class="s1">:aria-controls</span><span class="s2">=&quot;$id('month-dropdown')&quot;</span>
<a name="l63"><span class="ln">63   </span></a>                <span class="s1">&#64;click</span><span class="s2">=&quot;dayToggle()&quot;</span>
<a name="l64"><span class="ln">64   </span></a>                <span class="s1">type</span><span class="s2">=&quot;button&quot;</span>
<a name="l65"><span class="ln">65   </span></a>                <span class="s1">class</span><span class="s2">=&quot;inline-block hover:font-semibold px-1&quot;</span>
<a name="l66"><span class="ln">66   </span></a>            <span class="s0">&gt;</span>
<a name="l67"><span class="ln">67   </span></a>                <span class="s5">&#123;&#123; </span><span class="s6">$this</span><span class="s5">-&gt;</span><span class="s7">twoDigit</span><span class="s5">(</span><span class="s6">$day</span><span class="s5">) &#125;&#125;</span>
<a name="l68"><span class="ln">68   </span></a>            <span class="s0">&lt;/button&gt;</span>
<a name="l69"><span class="ln">69   </span></a>        <span class="s0">&lt;/div&gt;</span>
<a name="l70"><span class="ln">70   </span></a>
<a name="l71"><span class="ln">71   </span></a>        <span class="s4">&#123;&#123;-- Year Panel --&#125;&#125;</span>
<a name="l72"><span class="ln">72   </span></a>        <span class="s0">&lt;div</span>
<a name="l73"><span class="ln">73   </span></a>            <span class="s1">:id</span><span class="s2">=&quot;$id('year-dropdown')&quot;</span>
<a name="l74"><span class="ln">74   </span></a>            <span class="s1">style</span><span class="s2">=&quot;</span><span class="s1">display</span><span class="s3">: </span><span class="s2">none</span><span class="s8">;</span><span class="s2">&quot;</span>
<a name="l75"><span class="ln">75   </span></a>            <span class="s1">x-show</span><span class="s2">=&quot;yearIsOpen&quot;</span>
<a name="l76"><span class="ln">76   </span></a>            <span class="s1">&#64;click.outside</span><span class="s2">=&quot;yearClose()&quot;</span>
<a name="l77"><span class="ln">77   </span></a>            <span class="s1">class</span><span class="s2">=&quot;absolute left-1/2 -translate-x-1/2 w-60 border border-solid border-stone-400
<a name="l78"><span class="ln">78   </span></a>                   rounded-md bg-white mt-1 shadow&quot;</span>
<a name="l79"><span class="ln">79   </span></a>        <span class="s0">&gt;</span>
<a name="l80"><span class="ln">80   </span></a>            <span class="s0">&lt;ul </span><span class="s1">class</span><span class="s2">=&quot;grid grid-cols-3 text-center&quot;</span><span class="s0">&gt;</span>
<a name="l81"><span class="ln">81   </span></a>                <span class="s9">&#64;foreach(</span><span class="s6">$years </span><span class="s9">as </span><span class="s6">$y</span><span class="s9">)</span>
<a name="l82"><span class="ln">82   </span></a>                    <span class="s0">&lt;li</span>
<a name="l83"><span class="ln">83   </span></a>                        <span class="s1">&#64;click</span><span class="s2">=&quot;yearClose('month')&quot;</span>
<a name="l84"><span class="ln">84   </span></a>                        <span class="s1">wire:click</span><span class="s2">=&quot;$set('year', </span><span class="s5">&#123;&#123; </span><span class="s6">$years</span><span class="s5">[</span><span class="s6">$loop</span><span class="s5">-&gt;</span><span class="s7">index</span><span class="s5">] &#125;&#125;</span><span class="s2">)&quot;</span>
<a name="l85"><span class="ln">85   </span></a>                        <span class="s1">class</span><span class="s2">=&quot;inline-block py-1 hover:bg-stone-200 rounded-md cursor-pointer&quot;</span>
<a name="l86"><span class="ln">86   </span></a>                    <span class="s0">&gt;</span>
<a name="l87"><span class="ln">87   </span></a>                        <span class="s5">&#123;&#123; </span><span class="s6">$y </span><span class="s5">&#125;&#125;</span>
<a name="l88"><span class="ln">88   </span></a>                    <span class="s0">&lt;/li&gt;</span>
<a name="l89"><span class="ln">89   </span></a>                <span class="s9">&#64;endforeach</span>
<a name="l90"><span class="ln">90   </span></a>            <span class="s0">&lt;/ul&gt;</span>
<a name="l91"><span class="ln">91   </span></a>        <span class="s0">&lt;/div&gt;</span>
<a name="l92"><span class="ln">92   </span></a>        <span class="s4">&#123;&#123;-- Month Panel --&#125;&#125;</span>
<a name="l93"><span class="ln">93   </span></a>        <span class="s0">&lt;div</span>
<a name="l94"><span class="ln">94   </span></a>            <span class="s1">:id</span><span class="s2">=&quot;$id('month-dropdown')&quot;</span>
<a name="l95"><span class="ln">95   </span></a>            <span class="s1">style</span><span class="s2">=&quot;</span><span class="s1">display</span><span class="s3">: </span><span class="s2">none</span><span class="s8">;</span><span class="s2">&quot;</span>
<a name="l96"><span class="ln">96   </span></a>            <span class="s1">x-show</span><span class="s2">=&quot;monthIsOpen&quot;</span>
<a name="l97"><span class="ln">97   </span></a>            <span class="s1">&#64;click.outside</span><span class="s2">=&quot;monthClose()&quot;</span>
<a name="l98"><span class="ln">98   </span></a>            <span class="s1">class</span><span class="s2">=&quot;absolute left-1/2 -translate-x-1/2 w-80 border border-solid border-stone-400
<a name="l99"><span class="ln">99   </span></a>                   rounded-md bg-white mt-1 shadow&quot;</span>
<a name="l100"><span class="ln">100  </span></a>
<a name="l101"><span class="ln">101  </span></a>        <span class="s0">&gt;</span>
<a name="l102"><span class="ln">102  </span></a>            <span class="s0">&lt;ul </span><span class="s1">class</span><span class="s2">=&quot;grid grid-cols-3 text-center&quot;</span><span class="s0">&gt;</span>
<a name="l103"><span class="ln">103  </span></a>                <span class="s9">&#64;foreach(</span><span class="s6">$months </span><span class="s9">as </span><span class="s6">$m</span><span class="s9">)</span>
<a name="l104"><span class="ln">104  </span></a>                    <span class="s0">&lt;li</span>
<a name="l105"><span class="ln">105  </span></a>                        <span class="s1">&#64;click</span><span class="s2">=&quot;monthClose('day')&quot;</span>
<a name="l106"><span class="ln">106  </span></a>                        <span class="s1">wire:click</span><span class="s2">=&quot;$set('month', </span><span class="s5">&#123;&#123; </span><span class="s6">$loop</span><span class="s5">-&gt;</span><span class="s7">iteration </span><span class="s5">&#125;&#125;</span><span class="s2">)&quot;</span>
<a name="l107"><span class="ln">107  </span></a>                        <span class="s1">class</span><span class="s2">=&quot;inline-block py-1 hover:bg-stone-200 rounded-md cursor-pointer&quot;</span>
<a name="l108"><span class="ln">108  </span></a>                    <span class="s0">&gt;</span>
<a name="l109"><span class="ln">109  </span></a>                        <span class="s5">&#123;&#123; </span><span class="s6">$m </span><span class="s5">&#125;&#125;</span>
<a name="l110"><span class="ln">110  </span></a>                    <span class="s0">&lt;/li&gt;</span>
<a name="l111"><span class="ln">111  </span></a>                <span class="s9">&#64;endforeach</span>
<a name="l112"><span class="ln">112  </span></a>            <span class="s0">&lt;/ul&gt;</span>
<a name="l113"><span class="ln">113  </span></a>        <span class="s0">&lt;/div&gt;</span>
<a name="l114"><span class="ln">114  </span></a>        <span class="s4">&#123;&#123;-- Day Panel --&#125;&#125;</span>
<a name="l115"><span class="ln">115  </span></a>        <span class="s0">&lt;div</span>
<a name="l116"><span class="ln">116  </span></a>            <span class="s1">:id</span><span class="s2">=&quot;$id('day-dropdown')&quot;</span>
<a name="l117"><span class="ln">117  </span></a>            <span class="s1">style</span><span class="s2">=&quot;</span><span class="s1">display</span><span class="s3">: </span><span class="s2">none</span><span class="s8">;</span><span class="s2">&quot;</span>
<a name="l118"><span class="ln">118  </span></a>            <span class="s1">x-show</span><span class="s2">=&quot;dayIsOpen&quot;</span>
<a name="l119"><span class="ln">119  </span></a>            <span class="s1">&#64;click.outside</span><span class="s2">=&quot;dayClose()&quot;</span>
<a name="l120"><span class="ln">120  </span></a>            <span class="s1">class</span><span class="s2">=&quot;absolute flex justify-center left-1/2 -translate-x-1/2 w-72 border border-solid border-stone-400
<a name="l121"><span class="ln">121  </span></a>                   rounded-md bg-white mt-1 py-2 shadow cursor-default&quot;</span>
<a name="l122"><span class="ln">122  </span></a>        <span class="s0">&gt;</span>
<a name="l123"><span class="ln">123  </span></a>            <span class="s4">&#123;&#123;-- Week No. Column --&#125;&#125;</span>
<a name="l124"><span class="ln">124  </span></a>            <span class="s0">&lt;div </span><span class="s1">class</span><span class="s2">=&quot;grid grid-cols-1 px-1 mr-2&quot;</span><span class="s0">&gt;</span>
<a name="l125"><span class="ln">125  </span></a>                <span class="s0">&lt;div&gt;</span><span class="s10">&amp;nbsp;</span><span class="s0">&lt;/div&gt;</span>
<a name="l126"><span class="ln">126  </span></a>                <span class="s9">&#64;foreach(</span><span class="s6">$this</span><span class="s5">-&gt;</span><span class="s7">generateWeekNoList</span><span class="s5">(</span><span class="s6">$year</span><span class="s9">, </span><span class="s6">$month</span><span class="s5">) </span><span class="s9">as </span><span class="s6">$wno</span><span class="s9">)</span>
<a name="l127"><span class="ln">127  </span></a>                    <span class="s0">&lt;div </span><span class="s1">class</span><span class="s2">=&quot;text-stone-300 text-right&quot;</span><span class="s0">&gt;</span><span class="s5">&#123;&#123; </span><span class="s6">$wno </span><span class="s5">&#125;&#125;</span><span class="s0">&lt;/div&gt;</span>
<a name="l128"><span class="ln">128  </span></a>                <span class="s9">&#64;endforeach</span>
<a name="l129"><span class="ln">129  </span></a>            <span class="s0">&lt;/div&gt;</span>
<a name="l130"><span class="ln">130  </span></a>            <span class="s4">&#123;&#123;-- Calendar Column --&#125;&#125;</span>
<a name="l131"><span class="ln">131  </span></a>            <span class="s0">&lt;div&gt;</span>
<a name="l132"><span class="ln">132  </span></a>                <span class="s0">&lt;div </span><span class="s1">class</span><span class="s2">=&quot;flex items-center&quot;</span><span class="s0">&gt;</span>
<a name="l133"><span class="ln">133  </span></a>                    <span class="s9">&#64;foreach(</span><span class="s6">$weekDayHeaders </span><span class="s9">as </span><span class="s6">$wdh</span><span class="s9">)</span>
<a name="l134"><span class="ln">134  </span></a>                        <span class="s0">&lt;div </span><span class="s1">class</span><span class="s2">=&quot;px-2 w-8 font-bold text-center
<a name="l135"><span class="ln">135  </span></a>                                </span><span class="s9">&#64;if(</span><span class="s6">$loop</span><span class="s5">-&gt;</span><span class="s7">iteration </span><span class="s5">== </span><span class="s11">7</span><span class="s9">)</span>
<a name="l136"><span class="ln">136  </span></a>                                    <span class="s2">text-red-600
<a name="l137"><span class="ln">137  </span></a>                                </span><span class="s9">&#64;endif </span><span class="s2">&quot;</span>
<a name="l138"><span class="ln">138  </span></a>                        <span class="s0">&gt;</span><span class="s5">&#123;&#123; </span><span class="s6">$wdh </span><span class="s5">&#125;&#125;</span><span class="s0">&lt;/div&gt;</span>
<a name="l139"><span class="ln">139  </span></a>                    <span class="s9">&#64;endforeach</span>
<a name="l140"><span class="ln">140  </span></a>                <span class="s0">&lt;/div&gt;</span>
<a name="l141"><span class="ln">141  </span></a>                <span class="s0">&lt;div </span><span class="s1">class</span><span class="s2">=&quot;grid grid-cols-7&quot;</span><span class="s0">&gt;</span>
<a name="l142"><span class="ln">142  </span></a>                    <span class="s9">&#64;foreach(</span><span class="s6">$this</span><span class="s5">-&gt;</span><span class="s7">generateDayList</span><span class="s5">(</span><span class="s6">$year</span><span class="s9">, </span><span class="s6">$month</span><span class="s5">) </span><span class="s9">as </span><span class="s6">$d</span><span class="s9">)</span>
<a name="l143"><span class="ln">143  </span></a>                        <span class="s9">&#64;if(</span><span class="s6">$d </span><span class="s5">!= </span><span class="s11">0</span><span class="s9">)</span>
<a name="l144"><span class="ln">144  </span></a>                            <span class="s0">&lt;div</span>
<a name="l145"><span class="ln">145  </span></a>                                <span class="s1">&#64;click</span><span class="s2">=&quot;dayClose()&quot;</span>
<a name="l146"><span class="ln">146  </span></a>                                <span class="s1">wire:click</span><span class="s2">=&quot;$set('day', </span><span class="s5">&#123;&#123; </span><span class="s6">$d </span><span class="s5">&#125;&#125;</span><span class="s2">)&quot;</span>
<a name="l147"><span class="ln">147  </span></a>                                <span class="s1">class</span><span class="s2">=&quot;text-center hover:bg-stone-200 cursor-pointer
<a name="l148"><span class="ln">148  </span></a>                                </span><span class="s9">&#64;if(</span><span class="s6">$loop</span><span class="s5">-&gt;</span><span class="s7">iteration </span><span class="s5">% </span><span class="s11">7 </span><span class="s5">== </span><span class="s11">0</span><span class="s9">)</span>
<a name="l149"><span class="ln">149  </span></a>                                    <span class="s2">text-red-600
<a name="l150"><span class="ln">150  </span></a>                                </span><span class="s9">&#64;endif </span><span class="s2">&quot;</span>
<a name="l151"><span class="ln">151  </span></a>                            <span class="s0">&gt;</span><span class="s5">&#123;&#123; </span><span class="s6">$d </span><span class="s5">&#125;&#125;</span><span class="s0">&lt;/div&gt;</span>
<a name="l152"><span class="ln">152  </span></a>                        <span class="s9">&#64;else</span>
<a name="l153"><span class="ln">153  </span></a>                            <span class="s0">&lt;div </span><span class="s1">class</span><span class="s2">=&quot;text-center&quot;</span><span class="s0">&gt;</span><span class="s10">&amp;nbsp;</span><span class="s0">&lt;/div&gt;</span>
<a name="l154"><span class="ln">154  </span></a>                        <span class="s9">&#64;endif</span>
<a name="l155"><span class="ln">155  </span></a>                    <span class="s9">&#64;endforeach</span>
<a name="l156"><span class="ln">156  </span></a>                <span class="s0">&lt;/div&gt;</span>
<a name="l157"><span class="ln">157  </span></a>            <span class="s0">&lt;/div&gt;</span>
<a name="l158"><span class="ln">158  </span></a>        <span class="s0">&lt;/div&gt;</span>
<a name="l159"><span class="ln">159  </span></a>    <span class="s0">&lt;/div&gt;</span>
<a name="l160"><span class="ln">160  </span></a><span class="s0">&lt;/div&gt;</span></pre>
{{--                        <h2 class="font-bold text-xl">Tab 2 Content</h2>--}}
{{--                        <p class="mt-2 text-gray-500">Fugiat odit alias, eaque optio quas nobis minima reiciendis voluptate dolorem nisi facere debitis ea laboriosam vitae omnis ut voluptatum eos. Fugiat?</p>--}}
                    </section>
                </div>
            </div>
        </section>

    <script src="/js/app.js"></script>
    @livewireScripts
    </body>
</html>
