<?php

namespace App\Http\Livewire;

use Livewire\Component;

class DatePicker extends Component
{

    public array $yearRange;
    public int $year;
    public int $month;
    public int $day;
    public array $weekNumbers;
    public array $selectedDays;


    public function mount()
    {
        $now = getdate();
        $this->year = $now['year'];
        $this->month = $now['mon'];
        $this->day = $now['mday'];
    }

    public function updated($name, $value)
    {
        $this->selectedDays = $this->generateDayList($this->year, $this->month);
    }

    public function render()
    {

        return view('livewire.date-picker', [
            'years' => $this->generateYearList($this->yearRange),
            'months' => [
                'January', 'February', 'March',
                'April'  , 'May'     , 'June',
                'July'   , 'August'  , 'September',
                'October', 'November', 'December'
            ],
            'weekDayHeaders' => ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
        ]);
    }

    private function generateYearList($r): array {
        return range($r[0], $r[1]);
    }

    private function generateDayList($y, $m): array {
        $daysCount = cal_days_in_month(CAL_GREGORIAN, $m, $y);
        $firstDay = mktime(0, 0, 0, $m, 1, $y);
        $daysToAdd = [6, 0, 1, 2, 3, 4, 5][getdate($firstDay)['wday']];
        $result = range(1, $daysCount);
        for ($i = 0; $i < $daysToAdd; $i++) {
            array_unshift($result, 0);
        }
        return $result ;
    }

    private function generateWeekNoList($y, $m): array {
        $firstWeek = (int)idate('W', mktime(0, 0, 0, $m, 1, $y));
        $fieldsCount = count($this->generateDayList($y, $m));
        return range($firstWeek, $firstWeek + intdiv($fieldsCount, 7));
    }

    private function twoDigit(int $v): string {
        return ($v < 10) ? '0'.$v : (string)$v;
    }
}
